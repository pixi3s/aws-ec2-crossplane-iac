# Docker Dind Image
FROM docker:26.0.0-rc1-dind

# Install deps
RUN apk add --no-cache wget kubectl

# Install Kind
RUN wget -O /usr/local/bin/kind https://github.com/kubernetes-sigs/kind/releases/download/v0.11.1/kind-linux-amd64 && \
    chmod +x /usr/local/bin/kind

# Kubernetes version
ARG KUBERNETES_VERSION="v1.21.2"

# Run Docker daemon
RUN dockerd &

# Waiting 10 seconds
RUN sleep 10
